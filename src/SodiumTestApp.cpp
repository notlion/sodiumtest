#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

#include "sodium/sodium.h"
#include "boost/optional.hpp"

#include <unordered_map>
#include <numeric>

using namespace ci;
using namespace ci::app;
using namespace sodium;

using boost::optional;

template <typename T>
struct identity {
  typedef T type;
};

template <typename E, typename B>
event<E> gateIf(const event<E> &e, const behavior<B> &b,
                const typename identity<std::function<bool(const E &, const B &)>>::type &gated) {
  return filter_optional(e.template snapshot<B, optional<E>>(b, [gated](const E &ve, const B &vb) {
    return gated(ve, vb) ? optional<E>(ve) : optional<E>();
  }));
}

struct Touch {
  uint32_t id;

  vec2 position;
  vec2 startPosition;

  Touch(const TouchEvent::Touch &t, const vec2 &startPos)
  : id(t.getId()), position(t.getPos()), startPosition(startPos) {}
};

struct TouchGroupEvents {
  event<std::vector<Touch>> begins, moves, ends;
};

static vec2 calcTouchesCenter(const std::vector<Touch> &touches) {
  return std::accumulate(touches.begin(), touches.end(), vec2(), [](const auto &a, const auto &b) {
           return a + b.position;
         }) / float(touches.size());
}

static TouchGroupEvents groupTouches(size_t touchCount, const event<Touch> &begins,
                                     const event<Touch> &moves, const event<Touch> &ends) {
  using Touches = std::vector<Touch>;

  behavior_loop<Touches> touchesPrev;

  struct TouchesEdit {
    Touches touches;
    size_t index;
  };

  event<TouchesEdit> touchesIn =
      begins.snapshot<Touches, TouchesEdit>(touchesPrev, [](const auto &touch, auto touches) {
        touches.push_back(touch);
        return TouchesEdit{ touches, touches.size() - 1 };
      });
  event<TouchesEdit> touchesOut =
      ends.snapshot<Touches, TouchesEdit>(touchesPrev, [](const auto &touch, auto touches) {
        auto isThisTouch = [&](const auto &t) { return t.id == touch.id; };
        auto iter = std::find_if(touches.begin(), touches.end(), isThisTouch);
        touches.erase(iter);
        return TouchesEdit{ touches, size_t(iter - touches.begin()) };
      });
  event<TouchesEdit> touchesMove =
      moves.snapshot<Touches, TouchesEdit>(touchesPrev, [](const auto &touch, auto touches) {
        auto isThisTouch = [&](const auto &t) { return t.id == touch.id; };
        auto iter = std::find_if(touches.begin(), touches.end(), isThisTouch);
        *iter = touch;
        return TouchesEdit{ touches, size_t(iter - touches.begin()) };
      });

  auto touches = touchesIn.merge(touchesOut)
                     .merge(touchesMove)
                     .map<Touches>([](const auto &e) { return e.touches; })
                     .hold({});
  touchesPrev.loop(touches);

  auto isGroupEdit = [=](const auto &e) {
    return e.touches.size() >= touchCount && e.index < touchCount;
  };
  auto toGroup = [=](const auto &e) {
    return Touches(e.touches.begin(), e.touches.begin() + touchCount);
  };

  auto grpBegins = touchesIn.filter(isGroupEdit).map<Touches>(toGroup);
  auto grpMoves = touchesMove.filter(isGroupEdit).map<Touches>(toGroup);
  auto grpEnds = touchesOut.filter([=](const auto &e) {
                             return e.index < touchCount && e.touches.size() == touchCount - 1;
                           })
                     .snapshot<Touches, Touches>(touches, [](const auto &e, auto touches) {
                       std::copy(e.touches.begin(), e.touches.begin() + e.index, touches.begin());
                       if (e.index < e.touches.size()) {
                         std::copy(e.touches.begin() + e.index, e.touches.end(),
                                   touches.begin() + e.index + 1);
                       }
                       return touches;
                     });

  return { grpBegins, grpMoves, grpEnds };
}

struct Dot {
  vec2 pos;
  float radius;

  event_sink<Touch> touchBegins, touchMoves, touchEnds;

  event<Touch> presses, releases, taps, drags;

  behavior<int> activeTouchId = -1;

  behavior<bool> over = false;
  behavior<bool> pressed = false;

  static const Colorf colorDefault;
  behavior<Colorf> color = colorDefault;

  Dot(const vec2 &pos, float radius) : pos(pos), radius(radius) {
    auto activeTouchIdPrev = behavior_loop<int>();

    auto isActiveTouch = [](const Touch &t, int activeId) { return t.id == activeId; };

    presses = touchBegins;
    drags = gateIf(touchMoves, activeTouchIdPrev, isActiveTouch);
    releases = gateIf(touchEnds, activeTouchIdPrev, isActiveTouch);

    pressed = activeTouchIdPrev.map<bool>([](const auto &id) { return id >= 0; });
    auto notPressed = pressed.map<bool>(std::logical_not<bool>());

    activeTouchId =
        presses.map<int>([](const auto &t) { return t.id; }).merge(releases.map_to(-1)).hold(-1);

    activeTouchIdPrev.loop(activeTouchId);

    auto containsTouch = [this](const auto &t) { return contains(t.position); };

    // Clicks are releases where the touch is still in bounds
    taps = releases.filter(containsTouch);

    over = drags.merge(presses).map<bool>(containsTouch).merge(releases.map_to(false)).hold(false);
  }

  virtual ~Dot() {}

  bool contains(const vec2 &pnt) {
    return glm::distance(pos, pnt) < radius;
  }
};

const Colorf Dot::colorDefault = { 0.4f, 0.4f, 0.4f };

struct DotMomentary : public Dot {
  DotMomentary(const vec2 &pos, float radius) : Dot(pos, radius) {
    color =
        pressed.map<Colorf>([](bool pressed) { return pressed ? Colorf(1, 0, 0) : colorDefault; });
  }
};

struct DotSwitch : public Dot {
  DotSwitch(const vec2 &pos, float radius) : Dot(pos, radius) {
    auto wasOn = behavior_loop<bool>();
    auto on = taps.snapshot(wasOn).map<bool>([](bool on) { return !on; }).hold(false);
    wasOn.loop(on);
    color = on.map<Colorf>([](bool on) { return on ? Colorf(0, 1, 0) : colorDefault; });
  }
};

struct DotOver : public Dot {
  DotOver(const vec2 &pos, float radius) : Dot(pos, radius) {
    color = over.map<Colorf>([](bool over) { return over ? Colorf(0, 0, 1) : colorDefault; });
  }
};

struct DotDrag : public Dot {
  event<vec2> dragPositions;

  DotDrag(const vec2 &pos_, float radius_) : Dot(pos_, radius_) {
    color = pressed.map<Colorf>([](bool p) { return p ? Colorf(1, 0, 0) : colorDefault; });
    dragPositions = switch_e(presses.map<event<vec2>>([this](const auto &t) {
                                      auto offset = pos - t.position;
                                      return drags.map<vec2>(
                                          [offset](const auto &t) { return offset + t.position; });
                                    })
                                 .hold({}));
    dragPositions.listen([this](vec2 p) { pos = p; });
  }
};

struct DotDrag2 : public Dot {
  TouchGroupEvents twoTouches;
  event<vec2> dragPositions;

  DotDrag2(const vec2 &pos_, float radius_) : Dot(pos_, radius_) {
    twoTouches = groupTouches(2, touchBegins, touchMoves, touchEnds);

    auto twoPressed =
        twoTouches.begins.map_to(true).merge(twoTouches.ends.map_to(false)).hold(false);
    color = twoPressed.map<Colorf>([](bool p) { return p ? Colorf(1, 0, 1) : colorDefault; });

    dragPositions = switch_e(twoTouches.begins.map<event<vec2>>([this](const auto &touches) {
                                                auto offset = pos - calcTouchesCenter(touches);
                                                return twoTouches.moves.map<vec2>(
                                                    [offset](const auto &touches) {
                                                      return offset + calcTouchesCenter(touches);
                                                    });
                                              }).hold({}));
    dragPositions.listen([this](vec2 p) { pos = p; });

    // Debug
    auto str = [](const auto &ts) {
      std::string s;
      for (const auto &t : ts) {
        s += "(" + std::to_string(t.id) + " " + std::to_string(t.position.x) + " " +
             std::to_string(t.position.y) + ")" + ", ";
      }
      return s;
    };
    twoTouches.begins.listen(
        [=](const auto &ts) { std::cout << "begin: " << str(ts) << std::endl; });
    twoTouches.moves.listen([=](const auto &ts) { std::cout << "move: " << str(ts) << std::endl; });
    twoTouches.ends.listen([=](const auto &ts) { std::cout << "end: " << str(ts) << std::endl; });
  }
};

static const size_t kMaxTouches = 11;

class SodiumTestApp : public App {
public:
  void setup() override;
  void update() override;
  void draw() override;

  gl::BatchRef dotBatch;

  std::vector<std::unique_ptr<Dot>> dots;

  std::array<Dot *, kMaxTouches> touchOwners;
  std::array<vec2, kMaxTouches> touchStartPositions;
};

void SodiumTestApp::setup() {
  dotBatch = gl::Batch::create(geom::Circle().subdivisions(64),
                               gl::getStockShader(gl::ShaderDef().color()));

  dots.push_back(std::make_unique<DotMomentary>(getWindowCenter() + vec2(-100, 0), 50.0f));
  dots.push_back(std::make_unique<DotSwitch>(getWindowCenter() + vec2(100, 0), 50.0f));
  dots.push_back(std::make_unique<DotOver>(getWindowCenter() + vec2(0, -100), 50.0f));
  dots.push_back(std::make_unique<DotDrag>(getWindowCenter() + vec2(0, 100), 50.0f));
  dots.push_back(std::make_unique<DotDrag2>(getWindowCenter() + vec2(0, 300), 90.0f));

  std::fill(touchOwners.begin(), touchOwners.end(), nullptr);

  getWindow()->getSignalTouchesBegan().connect([this](const auto &e) {
    for (auto &appTouch : e.getTouches()) {
      for (auto it = dots.crbegin(); it != dots.crend(); ++it) {
        const auto &dot = *it;
        if (dot->contains(appTouch.getPos())) {
          auto touch = Touch(appTouch, appTouch.getPos());
          touchOwners.at(touch.id) = dot.get();
          touchStartPositions.at(touch.id) = touch.position;
          dot->touchBegins.send(touch);
          break;
        }
      }
    }
  });
  getWindow()->getSignalTouchesMoved().connect([this](const auto &e) {
    for (auto &appTouch : e.getTouches()) {
      auto owner = touchOwners.at(appTouch.getId());
      if (owner) {
        auto touch = Touch(appTouch, touchStartPositions.at(appTouch.getId()));
        owner->touchMoves.send(touch);
      }
    }
  });
  getWindow()->getSignalTouchesEnded().connect([this](const auto &e) {
    for (auto &appTouch : e.getTouches()) {
      auto owner = touchOwners.at(appTouch.getId());
      if (owner) {
        auto touch = Touch(appTouch, touchStartPositions.at(appTouch.getId()));
        touchOwners.at(touch.id) = nullptr;
        owner->touchEnds.send(touch);
      }
    }
  });
}

void SodiumTestApp::update() {}

void SodiumTestApp::draw() {
  gl::clear(Color(0, 0, 0));

  for (const auto &dot : dots) {
    gl::ScopedModelMatrix scopedModelMtx;
    gl::translate(dot->pos);
    gl::scale(vec2(dot->radius));
    gl::color(dot->color.sample());
    dotBatch->draw();
  }
}

CINDER_APP(SodiumTestApp, RendererGl, [](App::Settings *settings) {
  settings->setMultiTouchEnabled();
  settings->setHighDensityDisplayEnabled();
})
